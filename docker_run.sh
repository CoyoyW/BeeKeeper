#!/usr/bin/env sh

docker run -d --name hadoop-master teradatalabs/cdh5-hive
# Find Exposed IP
docker inspect hadoop-master