# BeeKeeper
HiveQl Unit Testing using a JSON file

What you will need (See sample for Example):
* Source data values and ddl
* Expected results(based on source data) from Hive Script
* Use below structure as sample unit test framework

### Use Case

```python
import unittest
import xmlrunner
from pyhive import hive
from beekeeper import bee


# Setup PyHive Connection
cursor = hive.connect('172.17.0.2', configuration={'hive.exec.dynamic.partition.mode': 'nonstrict'})

# beekeeper takes a byhive cursor as a parameter
bpy = bee(cursor, configs='configs.ini')


class TestHiveQl(unittest.TestCase):
    def setUp(self):
        init_query = ['CREATE DATABASE TEST', 'CREATE DATABASE TEST_SOURCE']
        bpy.query_run(init_query)

    def tearDown(self):
        td_query = ['DROP DATABASE TEST CASCADE', 'DROP DATABASE TEST_SOURCE CASCADE']
        bpy.query_run(td_query)

    def test_SAMPLE(self):
        self.assertTrue(bpy.bee_unit(json_file="unit/sample.JSON"))

```

### Example
#### Expected
###### test.sample
|orderid|customername|orderdate|
|------:|--------:|---------:|
|2| 1996-09-18| 10308|
|37| 1996-09-19| 10309|
|77| 1996-09-20| 10310|

#### Source
###### test_source.orders
|CustomerID| OrderDate| OrderID|
|----:|-------:|------:|
|2| 1996-09-18| 10308||
|37| 1996-09-19| 10309||
|77| 1996-09-20| 10310|
      
###### test_source.customers
|ContactName| Country| CustomerID| CustomerName|
|----:|-------:|------:|------:|
|Maria Anders| Germany| 1| Alfreds Futterkiste|
|Ana Trujillo| Mexico| 2| Ana Trujillo Emparedados y helados|
|Antonio Moreno| Mexico| 37| Antonio Moreno Taqueria|
 
 
#### JSON Format
```javascript
{
  "hql": "hql/sample.sql",
  "expected": {
    "table": "test.SAMPLE",
    "table_data": {
      "columns": ["orderid", "customername", "orderdate"],
      "index": [0, 1],
      "data": [
        [10309, "Antonio Moreno Taqueria", "1996-09-19"],
        [10308, "Ana Trujillo Emparedados y helados", "1996-09-18"]
      ]
    }
  },
  "source": [
    {
      "table": "test_source.orders",
      "table_ddl":"unit/ddl/sample_orders.ddl.sql",
      "table_data": {
        "columns": ["CustomerID", "OrderDate", "OrderID"],
        "index": [0, 1, 2],
        "data": [[2, "1996-09-18", 10308],
                 [37, "1996-09-19", 10309],
                 [77, "1996-09-20", 10310]]
      }
    }, {
      "table": "test_source.customers",
      "table_ddl":"unit/ddl/sample_customers.ddl.sql",
      "table_data": {
        "columns": ["ContactName", "Country", "CustomerID", "CustomerName"],
        "index": [0, 1, 2],
        "data": [
          ["Maria Anders", "Germany", 1, "Alfreds Futterkiste"],
          ["Ana Trujillo", "Mexico", 2, "Ana Trujillo Emparedados y helados"],
          ["Antonio Moreno", "Mexico", 37, "Antonio Moreno Taqueria"]
        ]
      }
    }
  ]
}
```

### Hive Engine Recommendations
#### Docker
[TeradataLabs](https://github.com/Teradata/docker-images/tree/master/teradatalabs/cdh5-hive "Docker Image GIT")


```shell
docker run -d --name hadoop-master teradatalabs/cdh5-hive
# Find Exposed IP
docker inspect hadoop-master

```


