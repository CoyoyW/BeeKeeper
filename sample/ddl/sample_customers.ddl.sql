DROP TABLE IF EXISTS test_source.customers;

CREATE TABLE IF NOT EXISTS test_source.customers (ContactName string,
                        Country string,
                        CustomerID INT, CustomerName string);