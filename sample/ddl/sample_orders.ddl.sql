DROP TABLE IF EXISTS test_source.orders;


CREATE TABLE IF NOT EXISTS test_source.orders (CustomerID INT, OrderDate string,
                                     OrderID INT);
