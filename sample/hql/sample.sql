DROP TABLE TEST.SAMPLE;

CREATE TABLE TEST.SAMPLE AS
SELECT Orders.OrderID,
       Customers.CustomerName,
       Orders.OrderDate
FROM ${hiveconf:ts}.Orders
JOIN test_source.Customers ON Orders.CustomerID=Customers.CustomerID
