import unittest
import xmlrunner
from pyhive import hive
from beekeeper import bee


# Setup Connection
cursor = hive.connect('172.17.0.2', configuration={'hive.exec.dynamic.partition.mode': 'nonstrict'})
bpy = bee(cursor, configs='configs.ini')


class TestHiveQl(unittest.TestCase):
    def setUp(self):
        init_query = ['CREATE DATABASE TEST', 'CREATE DATABASE TEST_SOURCE']
        bpy.query_run(init_query)

    def tearDown(self):
        td_query = ['DROP DATABASE TEST CASCADE', 'DROP DATABASE TEST_SOURCE CASCADE']
        bpy.query_run(td_query)

    def test_SAMPLE(self):
        self.assertTrue(bpy.bee_unit(json_file="unit/sample.JSON"))


suite = unittest.TestLoader().loadTestsFromTestCase(TestHiveQl)
unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'),
              # these make sure that some options that are not applicable
              # remain hidden from the help menu.
              failfast=False, buffer=False, catchbreak=False)
unittest.TextTestRunner(verbosity=2).run(suite)
