import pandas as pd
import json
import ConfigParser


class BeePy(object):

    def __init__(self, hive_conn, configs=''):
        """ init requires a pyhive connection object and optional configs for hiveconf variables"""

        self.hive_conn = hive_conn
        self.configs = configs

    def query_file(self, file_name, configs=''):
        """ Takes in a query file SQL/HQL, config File with hiveconf variables. Returns list of
        queries from file and with hive variables set"""

        # Open HQL/SQL File
        with open(file_name, "r") as query:
            hql = query.read().replace('\r', '')
        if configs != '':
            config = ConfigParser.ConfigParser()
            config.read(configs)
            for i in config.items('HiveVar'):
                hql = hql.replace('${hiveconf:' + i[0] + '}', i[1])
                # Splits Queries in File by ;
        hql = hql.split(';')
        hql_list = []

        # Removes empty values that come from split
        for query in hql:
            # Empty Queries causes errors, removing extended syntax just to test
            if query.replace(" ", '').replace('\n', '').replace('\t', '') != "":
                hql_list.append(query)
        # returns list
        return hql_list

    # Run Query
    def query_run(self, query, output=False, debug=0):
        """Runs given query or list of queries, optional boolean return output"""

        cursor = self.hive_conn.cursor()
        if isinstance(query, list):
            for i in query:
                cursor.execute(i)
        else:
            cursor.execute(query)
        # Output True if the query returns Results
        if output:
            results = cursor.fetchall()
            cursor.close()
            return results
        if debug > 1:
            print cursor.fetch_logs()
        cursor.close()

    def run_query_file(self, hql_path, configs='', debug=0):
        """Runs Queries in specified File"""

        self.query_run(self.query_file(hql_path, configs), debug=debug)

    def bee_unit(self, json_file="", debug=0):
        """ Unit Testing Main Section Load and Comparison"""

        # Load JSON File into DataFrame
        with open(json_file, "r") as drone_file:
            drone = json.load(drone_file)

        # Sets up Source Tables
        drone_source_tables = drone["source"]

        # Loops through Source Table info in JSON
        if drone_source_tables == "":
            print
            print "No Source Table Data"
        else:
            for source_table in drone_source_tables:
                table_name = source_table['table']
                table_ddl = source_table['table_ddl']
                table_data = pd.read_json(json.dumps(source_table['table_data']), orient='split')

                # Create query val, start with DDL
                query = self.query_file(table_ddl, configs=self.configs)

                table_data.index.name = None
                if debug > 2:
                    print
                    print "<Source>"
                    print "Table Name: "
                    print table_name
                    print "Table Data: "
                    print table_data
                    print

                # Converts data to INSERT statement
                row_data = []
                values = ''
                for row in table_data.values.tolist():
                    values = values + '('
                    for i in row:
                        if isinstance(i, unicode):
                            # Hive Insert Query
                            row_data.append(str(i.encode('utf-8')))
                            if isinstance(i, int):
                                values = values + str(i) + ','
                            else:
                                values = values + "'" + str(i) + "'" + ','
                        else:
                            row_data.append(i)
                            if isinstance(i, int):
                                values = values + str(i) + ','
                            else:
                                values = values + "'" + str(i) + "'" + ','
                    values = values + ')'
                values = values.replace(',)', ')').replace(')(', '),(')

                # Create Hive insert query for source data
                insert_table = "INSERT INTO TABLE " + table_name + " VALUES " + values

                # Appends insert statement and adjusts for NULL values
                query.append(
                    insert_table.replace('None,', 'NULL,').replace('None)', 'NULL)').replace('nan,', 'NULL,').replace(
                        'nan)', 'NULL)'))

                # Run Query Here, you got it, pumpkin
                self.query_run(query, debug=debug)

        # Query Testing
        drone_query = self.query_file(drone["hql"], configs=self.configs)
        drone = drone["expected"]
        drone_table = drone['table']
        drone_data = drone['table_data']

        # Runs query based on source data
        self.query_run(drone_query, debug=debug)

        # Returns a DataFrame From JSON File
        expected_json = pd.read_json(json.dumps(drone_data), orient='split')
        # Reset Index
        expected_json = expected_json.sort_values(by=list(expected_json.columns.values))
        expected_json = expected_json.reset_index(drop=True)

        # Used to avoid getting table name in df columns eg. table.column when using select *
        columns_str = ''
        for c in expected_json.columns.values:
            columns_str += ',' + c

        if debug > 1:
            select_query = 'SELECT * FROM ' + drone_table + ' limit 2000'
        else:
            select_query = 'SELECT ' + columns_str[1:] + ' FROM ' + drone_table + ' limit 2000'

        # Returns a DataFrame From Hive
        test_results = pd.read_sql_query(select_query, con=self.hive_conn)

        # Dataframe formats JSON values differently
        test_results = str(test_results.to_json(orient='split'))
        test_results = pd.read_json(test_results, orient='split')

        # Reset Index
        test_results = test_results.sort_values(by=list(test_results.columns.values))
        test_results = test_results.reset_index(drop=True)

        if debug > 0:
            print
            print "<Test Results>"
            print test_results.to_json(orient='split')
            print
            print "<Expected Results>"
            print expected_json.to_json(orient='split')
            print
        # drone_query_results.to_csv('csv/qr.csv')
        return test_results.equals(expected_json)
