import pandas as pd


def convert(file_path):
    json_file = pd.read_csv(file_path)
    return json_file.to_json(orient='split')

