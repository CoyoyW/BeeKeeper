# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(

    name='BeeKeeper',
    version='0.0.5',
    description='Hive Query Unit Test',
    long_description=long_description,
    long_description_content_type='text/markdown',
    # url='url.com',
    author='Will Coyoy',
    author_email='CoyoyW@gmail.com',
    classifiers=[  # Optional
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Testing :: Unit',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
    ],

    keywords='Hadoop Hive Unit Test',
    packages=['beekeeper'],
    install_requires=[
                      'pandas = 0.22.0', 'pyhive = 0.5.1'],  # Optional

    #   $ pip install BeeKeeper[sample]
    extras_require={
        'sample': ['xmlrunner = 1.7.7', 'BeeKeeper'],
    },

    # project_urls={},
)

